import React, { Component } from 'react';
import { Jumbotron} from 'reactstrap';
import Image from './Image.js';
import Helpers from './../utils/Helpers.js';

class CityWeather extends Component {

  constructor(props) {
      super(props);
  }
  
  render() {
    // loop weather data to get weather on different times/days
    var weather = this.props.weather;
    let list = weather.consolidated_weather.map(function(cityWeather) {
      return (<div className="weather col-md-4" key={cityWeather.id} >
                <Jumbotron className="list-jumbo">
                  <p>{ Helpers.getDay(cityWeather.time) }</p>
                  <h3 >Weather: {Helpers.roundDecimal(cityWeather['the_temp'])}&#8451;</h3>
                  <Image imageUri={Helpers.getWeatherIcon(cityWeather['weather_state_abbr'])}/>
                  <hr className="my-2" />
                  <p className="lead">Maximum Temperature: {Helpers.roundDecimal(cityWeather['max_temp'])}&#8451;</p>
                  <p className="lead">Minimum Temperature: {Helpers.roundDecimal(cityWeather['min_temp'])}&#8451;</p>                    
                </Jumbotron>
              </div>
      );
    });
      return (
          <div className="weather col-md-12" >
                            
            <Jumbotron>
              <h2 >{weather.title}, {weather.parent['title']}</h2>
              <p>Timezone: {weather.timezone_name} - {weather.timezone}</p>
              <div className="row">
                { list }
              </div>
            </Jumbotron>
        
        </div>
        );
    
  }
}
  

export default CityWeather;
