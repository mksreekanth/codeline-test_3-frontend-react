import React, { Component } from 'react';

class Image extends Component {

  render() {
        return <img className="weather-img"  ref="image" src={this.props.imageUri}  />;        
  }
}  
export default Image;
