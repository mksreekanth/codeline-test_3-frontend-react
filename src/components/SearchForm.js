import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';


class SearchForm extends Component {

	constructor(props) {
      super(props);
    }


	render() {
		const {value, onChange} = this.props;
	    return (<div className="searchform">
	    			<div className="input-group">
		    			  <input type="text" className="form-control" 
		    			  value={this.props.inputValue} onChange={this.props.valueChanged} placeholder="Search weather by city" />
		    			  <span className="input-group-btn">
		    			  	<Link to={ {
		    			  	            pathname: '/search/:'+this.props.inputValue,
		    			  	            
		    			  	          } } replace>
		    			    	<Button>Search</Button>
		    			    </Link>
		    			  </span>
	    			</div>
		        </div>
		    );        
	}
}  
export default SearchForm;
