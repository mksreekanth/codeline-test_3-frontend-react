import React, { Component } from 'react';
import './../App.css';
import axios from 'axios';
import Weather from './Weather.js';
import SearchForm from './SearchForm.js';
import { HashRouter, Link } from 'react-router-dom'


class List extends Component {
    constructor(props) {
      super(props);
      this.handleFieldChange = this.handleFieldChange.bind(this);
      this.state = {cities: ["Istanbul", "Berlin", "London", "Helsinki", "Dublin", "Vancouver" ], 
      woeid:[], weather: [], searchQuery: '', loading: true, isMounted: false};
    }
    
    // get woeid of cities, set as state, then get weather in all cities
  getWoeid(city){
    
    axios.get('http://localhost/weather.php?command=search&keyword='+city)
      .then(response => {
        let result = [];
        result = response.data[0];
        this.setState(prevState => ({
            woeid: this.state.woeid.concat([result]),
          }));
          return axios.get('http://localhost/weather.php?command=location&woeid='+result['woeid']);
            

      })
      .then(response => {
          this.setState(prevState => ({
              weather: this.state.weather.concat([response.data]),
              loading: false
            }));
      })
              
      .catch((error)=>{
        console.log(error);
    });

  }

  
  componentDidMount() {
    
    this.setState( { isMounted: true }, () => {
        this.state.cities.forEach(city => 
              this.getWoeid(city)
            );
    } ); 

  }


  componentWillUnmount() {
    this.setState( { isMounted: false } )
  }

  // handle search field input change
  handleFieldChange(e) {
    this.setState({ searchQuery: e.target.value });
  }


  render() {
    if (this.state.loading === false) {
      
      // loop cities to makr tiles for each cities
      let list = this.state.weather.map(function(cityWeather) {
      return (<div className="weather col-md-4" key={cityWeather.title} >
                <HashRouter>
                  <Link to={ {
                      pathname: '/weather/:'+cityWeather.woeid,
                      
                    } }>
                    <Weather weather={cityWeather} key = {cityWeather.woeid}/>
                  </Link>
                </HashRouter>
              </div>
          
      );
    });

      return( 
        <div className="container">
            <SearchForm valueChanged={ this.handleFieldChange } inputValue = { this.state.searchQuery } />
            <div className="row">
              { list }
            </div>
        </div>
        );
    }
    else {
      return <h3>Loading...</h3>;
    }
  }
}

export default List;