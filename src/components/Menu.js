import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import { Button, Nav, NavItem } from 'reactstrap';
import { Link } from 'react-router-dom';



class Menu extends Component {
    

  render() {

        return(
          <div className="Nav">
            <Nav pills>
              <NavItem>
                <Link to={ {
                            pathname: '/'
                          } } replace>
                    <Button>Home</Button>
                </Link>
              </NavItem>
            </Nav>
          </div>
      );
    
  }
}

export default Menu;


