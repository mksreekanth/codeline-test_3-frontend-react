import React, { Component } from 'react';
import { Jumbotron } from 'reactstrap';
import Image from './Image.js';
import Helpers from './../utils/Helpers.js';


class Weather extends Component {

  render() {
    const weather = this.props.weather;
    
      
        let cityWeather = this.props.weather;
      return (
        
            <Jumbotron>
              <p>{ Helpers.getDay(cityWeather.consolidated_weather[5]['time']) }</p>
              <h2 >{cityWeather.title}, {cityWeather.parent['title']}</h2>
              <h3 >Weather: {Helpers.roundDecimal(cityWeather.consolidated_weather[5]['the_temp'])}&#8451;</h3>
              <Image imageUri={Helpers.getWeatherIcon(cityWeather.consolidated_weather[5]['weather_state_abbr'])}/>
              <hr className="my-2" />
              <p className="lead">Maximum Temperature: {Helpers.roundDecimal(cityWeather.consolidated_weather[5]['max_temp'])}&#8451;</p>
              <p className="lead">Minimum Temperature: {Helpers.roundDecimal(cityWeather.consolidated_weather[5]['min_temp'])}&#8451;</p>                    
            </Jumbotron>
          
        );
  
  }
}
  
       


export default Weather;
