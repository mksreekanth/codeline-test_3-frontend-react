import React, { Component } from 'react';
import axios from 'axios';
import { Progress } from 'reactstrap';
import CityWeather from './CityWeather.js';

class City extends Component {

  constructor(props) {
      super(props);
      this.state = {weather: [], loading: true};
  }

  // get weather using API
  getweather() {
    let woeid = this.props.location.pathname.split(":").slice(-1)[0];
    axios.get('http://localhost/weather.php?command=location&woeid='+woeid)
      .then(response => { this.setState({weather: response.data, loading: false });
      })          
      .catch((error)=>{
        console.log(error);
    });

  }


  componentDidMount() {
      
      this.getweather();  
  }
  

  render() {
    if (this.state.loading === false) {
      // loading complete. display cityweather component
      return (
          <CityWeather weather={this.state.weather} />
        );
    }
    // still loading
    else{
      return (
        <div>
          <p>LOADING...</p>
          <Progress multi>
            <Progress bar value="15" />
            <Progress bar color="success" value="30" />
            <Progress bar color="info" value="25" />
            <Progress bar color="warning" value="20" />
            <Progress bar color="danger" value="5" />
          </Progress>
        </div>
      );
    }
  }
}
  
       


export default City;
