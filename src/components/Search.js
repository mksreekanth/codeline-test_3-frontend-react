import React, { Component } from 'react';
import axios from 'axios';
import { Progress } from 'reactstrap';
import { Link } from 'react-router-dom';
import SearchForm from './SearchForm.js';
import { HashRouter } from 'react-router-dom'
import Weather from './Weather.js';

class City extends Component {

  constructor(props) {
      super(props);
      this.handleFieldChange = this.handleFieldChange.bind(this);
      this.state = {weather: [], loading: true, api_success: false, searchQuery: ''};
  }

  // get weather in searched city
  getweather() {
    let query = this.props.location.pathname.split(":").slice(-1)[0];
    axios.get('http://localhost/weather.php?command=search&keyword='+query)
      .then(response => {
       
          if (response.data.length !== 0) {
            return axios.get('http://localhost/weather.php?command=location&woeid='+response.data[0]['woeid'])
          }
          else{
            this.setState({
                api_success: 'no_result',
              });
            throw new Error('');
          }
        

      }) 
      .then(response => {
          this.setState({
              weather: response.data,
              loading: false,
              api_success: 'rendered',
            });
      })         
      .catch((error)=>{
        console.log(error);
    });
    this.setState({
        api_success: true,
      });

  }


  // handle input field change 
  handleFieldChange(e) {
    this.setState({ searchQuery: e.target.value });
    
  }

  componentWillReceiveProps(nextProps){
    window.location.reload();
  }


  componentDidMount() {
      
      this.getweather();
      this.setState({
          searchQuery: this.props.location.pathname.split(":").slice(-1)[0],
        });  
  }
  
  render() {
    if (this.state.loading === false && this.state.api_success ==='rendered' && this.state.weather.length !== 0 ) {
      return (
          <div className = "seacrh-container">
            <SearchForm valueChanged={ this.handleFieldChange } inputValue = { this.state.searchQuery } />
            <div className="weather col-md-12"  >
              <HashRouter>
                <Link to={ {
                    pathname: '/weather/:'+this.state.weather.woeid,
                    
                  } }>
                  <Weather weather={this.state.weather} />
                </Link>
              </HashRouter>
            </div>
            
          </div>
        );
    }
    // no results
    else if (this.state.api_success ==='no_result' ) {
      return (
        <div>
          <SearchForm valueChanged={ this.handleFieldChange } inputValue = { this.state.searchQuery } />
          <h3>No results were found. Try changing the keyword!”</h3>
          
        </div>
      );
    }
    // loading weather
    else{
      return (
        <div>
          <p>LOADING...</p>
          <Progress multi>
            <Progress bar value="15" />
            <Progress bar color="success" value="30" />
            <Progress bar color="info" value="25" />
            <Progress bar color="warning" value="20" />
            <Progress bar color="danger" value="5" />
          </Progress>
        </div>
      );
    }
  }
}
  
       


export default City;
