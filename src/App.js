import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Menu from './components/Menu.js';
import List from './components/List.js';
import City from './components/City.js';
import Search from './components/Search.js';
import { Route, IndexRoute, hashHistory, Switch } from 'react-router'
import { HashRouter } from 'react-router-dom'


class App extends Component {
    

  render() {
        return(
          <div className="App">
            <HashRouter>  
              <div>   
                <Menu />  
                <Switch>
                  <Route exact path="/" component={List} /> 
                  <Route path="/weather" component={City} />
                  <Route path="/search" component={Search} />
                </Switch>
              </div>
            </HashRouter>
            
            
          </div>
      );
    
  }
}

export default App;


