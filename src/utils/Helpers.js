class Helpers {
	
	// round weather to two decimal places
	static roundDecimal(num) {
	  return Math.round(num * 100) / 100;
	}

	// get weather icon from metaweather
	static getWeatherIcon(weather) {
	    return 'https://www.metaweather.com/static/img/weather/'+weather+'.svg';
	}

	// get day in the format to be displayed in the weather tile
	static getDay(date) {
	    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	    var d = new Date("2018-09-26T23:59:52.756018Z");
	    var dayName = days[d.getDay()];
	    var monthName = months[d.getMonth()];
	    return( dayName+', '+d.getDate()+' '+monthName+' '+d.getFullYear());
	}	
	
}
export default Helpers;